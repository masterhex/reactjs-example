import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import './Items.css';
//import { Collapse, Button, CardBody, Card } from 'reactstrap'
//import ToolTip from 'react-portal-tooltip'
import Items from './components/Items'
import { Button } from 'semantic-ui-react'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textareaValue : "",
      items: [{
        id: 1,
        type: "container",
        items: [

        ]
      }],
      counter: 0
    };
  }

  refresh = () => {
    this.setState({ items: this.items });
  }

  refreshItems = () => {
    this.setState({ items: this.state.items });
  }

  incCounter = () => {
    console.log(this.state.counter);
    this.setState(prevState => {
      return {counter: prevState.counter + 1}
   })
    console.log(this.state.counter);
    return this.state.counter;
  }

  createJson = () => {
    var items = JSON.stringify(this.state.items);
    this.setState({ textareaValue: items });
  }

  build = () => {
    if (this.state.textareaValue===""){
      this.setState({ items: [{
        id: 1,
        type: "container",
        items: [
        ]
      }] });
      return;
    }

    var items = JSON.parse(this.state.textareaValue);
    this.setState({ items: items });
  }

  handleChange = (event) => {
    this.setState({textareaValue: event.target.value});
  }

  render() {
    //const classes = 'tooltip-inner'
    return (
      <div className="App">
        <p>Place your JSON here:</p>
        <textarea type="text" cols={100} rows={15} value={this.state.textareaValue} onChange={this.handleChange} />
        <br></br>
        <Button primary onClick={() => this.build()}>Build</Button>
        <Button secondary onClick={() => this.createJson()}>Create JSON</Button>
        <Items items={this.state.items} refreshItems={this.refreshItems} />
      </div>
    )
  }
}

export default App;
