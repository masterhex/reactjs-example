import React, { Component } from 'react'
import Box from './Box'


import TooltipComponent from './TooltipComponent';

export default class Items extends Component {

  refreshItems = () => {
    console.log(typeof this.props.refreshItems);
    if (typeof this.props.refreshItems === 'function') {
      this.props.refreshItems();
    }
  }
  incCounter = () => {
    return this.props.incCounter();
  }

  render() {
    const { items } = this.props

    return (
      <div className="items" style={{margin: "20px", padding : "20px"}}>

        {items.map(item =>
          item.type === "container" ?
            <div key={item.id} className="item" style={{ border: "2px solid", display: "inline-flex" }}>

              {item.items && <Items items={item.items} refreshItems={this.refreshItems} />}
              <TooltipComponent id={"item" + item.id} item={item} refreshItems={this.refreshItems}
                 items={items} />
            </div>

            :
            <Box key={item.id} item={item} />
        )}
      </div>
    )


  }



}

