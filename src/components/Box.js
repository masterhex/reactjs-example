import React, { Component } from 'react'

export class Box extends Component {
    constructor(props) {
        super(props);
        this.changeColor = this.changeColor.bind(this);
        this.state = {
            item: this.props.item,
            color: this.props.item.color,
            colors: [
                "red", "blue", "green", "orange"
            ]
        }

    }


    changeColor = (item) => {

        var index = this.state.colors.findIndex(x => x === item.color);

        if (index === this.state.colors.length - 1) {
            item.color = this.state.colors[0]
        } else {
            item.color = this.state.colors[index + 1]
        }
        this.setState({ item: item })

    }

    render() {
        const { item } = this.props
        return (
            <div key={item.id} className="item" style={{
                border: "2px solid",
                backgroundColor: item.color, width: "100px", height: "100px", margin: "5px"
            }}
                onClick={() => this.changeColor(item)}>


            </div>
        )
    }
}

export default Box
