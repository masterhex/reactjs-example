import React, { Component } from 'react'
import ToolTip from 'react-portal-tooltip'

import { Button } from 'semantic-ui-react'


export class TooltipComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.item,
      isTooltipActive: false
    }
    this.incCounter = this.incCounter.bind(this);
  }

  showTooltip() {
    this.setState({ isTooltipActive: true })
  }
  hideTooltip() {
    this.setState({ isTooltipActive: false })
  }

  addBox = (item) => {
    var newId = Math.random().toString(36).substring(7);
    item.items.push({ id: newId, type: "box", color: "red" });
    this.refreshItems();
  }
  addContainer = (item) => {

    var newId = Math.random().toString(36).substring(7);
    item.items.push({ id: newId, type: "container", items: [] });
    this.refreshItems();
  }

  incCounter = () => {
    return this.props.incCounter();
  }
  refreshItems = () => {
    if (typeof this.props.refreshItems === 'function') {
      this.props.refreshItems();
    }
  }

  render() {
    const id = this.props.id;
    const item = this.props.item;
    return (
      <div>
        <Button style={{margin: "20px"}} id={id} onMouseEnter={this.showTooltip.bind(this)} onMouseLeave={this.hideTooltip.bind(this)}>Add</Button>
        <ToolTip active={this.state.isTooltipActive} position="bottom" arrow="center" parent={"#" + id}>
          <div>
            <Button onClick={() => this.addBox(item)}>
              Box
            </Button>
            <Button onClick={() => this.addContainer(item)}>
              Container
            </Button>
          </div>
        </ToolTip>


      </div>
    )
  }
}

export default TooltipComponent
